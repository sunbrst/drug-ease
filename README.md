# drug-ease
#### Step 0:
Install Python3
https://www.python.org/downloads/release

`pip install flask_restful` and 
`pip install httpie`
#### Step 1:
run this:
`python pharm_inventory.py`

#### Step 2. Try it out and see the updates to the json:
`http -v GET "http://127.0.0.1:5000/inventory/<insert-location-name>"`

example:
`http -v GET "http://127.0.0.1:5000/inventory/StJoes"`
or 
`http -v GET "http://127.0.0.1:5000/inventory/StJoes?status=Incoming"`
`http -v PUT "http://127.0.0.1:5000/inventory/StJoes?name=SomeDrug&exp=2010-01-01&qty=15&units=kg"`


TODO: Convert the JSON file to a database
TODO: Checks if we go below 0 on inventor
TODO: Convert units
TODO: Maybe put the location as a query as well

