from flask import Flask
from flask_restful import Api, Resource, reqparse
from flask_swagger_ui import get_swaggerui_blueprint
# from routes import request_api
import json

class Inventory(Resource):
    def get(self, location_name):
        some_drugs = all_drugs[location_name]
        parser = reqparse.RequestParser()
        parser.add_argument("status")
        parser.add_argument("name")
        args = parser.parse_args()
        if not args['status']:
            args['status'] = 'Inventory'
        return_drugs = []
        for drug in some_drugs[args['status']]:
            if args['name']:
                if args['name'] == drug['name']:
                    return_drugs.append(drug)
            else:
                return_drugs.append(drug)
        if len(return_drugs) > 0:
            return return_drugs, 200
        return "No drugs found", 400

    def post(self, location_name):
        some_drugs = all_drugs[location_name]
        parser = reqparse.RequestParser()
        parser.add_argument("name")
        parser.add_argument("exp")
        parser.add_argument("units")
        parser.add_argument("qty")
        parser.add_argument("status")
        args = parser.parse_args()
        if not args['name']:
            return 'Bad Request: Need a drug name', 400
        if not args['status']:
            args['status'] = 'Inventory'
        for drug in some_drugs[args['status']]:
            if args['name'] == drug['name']:
                if args['units'] == drug['units']:
                    if args['exp'] == drug['exp']:
                        drug['qty'] += float(args['qty'])
                        all_drugs[location_name] = some_drugs
                        update_json()
                        return drug, 200
        drug = { "name" : args['name'],
                "units" : args['units'],
                "qty" : float(args['qty']),
                "exp" : args['exp']
                }
        some_drugs[args['status']].append(drug)
        all_drugs[location_name] = some_drugs
        update_json()
        return drug, 201

    def put(self, location_name):
        some_drugs = all_drugs[location_name]
        parser = reqparse.RequestParser()
        parser.add_argument("exp")
        parser.add_argument("units")
        parser.add_argument("qty")
        parser.add_argument("name")
        parser.add_argument("status")
        args = parser.parse_args()
        if not args['name']:
            return 'Bad Request: Need a drug name', 400
        if not args['status']:
            args['status'] = 'Inventory'
        for drug in some_drugs[args['status']]:
            if args['name'] == drug['name']:
                drug['qty'] = float(args['qty'])
                drug['units'] = args['units']
                drug['exp'] = args['exp']
                all_drugs[location_name] = some_drugs
                update_json()
                return drug, 200
        return f"Could not find {name}", 400

def update_json():
    print("Putting stuff back in the JSON file")
    print(json.dumps(all_drugs))
    with open(json_file, 'w+') as wf:
        json.dump(all_drugs, wf)

def main():
    ## ~~~~~~~~~~ CONFIG  ~~~~~~~~~~
    global all_drugs, json_file
    json_file = 'drug_data.json'
    #TODO: remove this
    with open(json_file, 'r') as rf:
        all_drugs = json.load(rf)
    SWAGGER_URL = '/swagger'
    API_URL = '/static/swagger.json'
    swaggerui_blueprint = get_swaggerui_blueprint(
                            SWAGGER_URL,
                            API_URL,
                            config = {"app_name":"drug_ease"})
    ## ~~~~~~~~~~ CONFIG  ~~~~~~~~~~

    app = Flask(__name__)
    app.register_blueprint(swaggerui_blueprint, url_prefix = SWAGGER_URL)
    # app.register_blueprint(request_api.get_blueprint())
    api = Api(app)
    api.add_resource(Inventory, "/inventory/<string:location_name>")
    app.run(debug = True)


if __name__ == "__main__":
    main()
